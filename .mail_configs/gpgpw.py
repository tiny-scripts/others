#!/usr/bin/python

import os
import subprocess

def get_password_gpg(server):
  acct = os.path.basename(server)
  path = "/home/shaiton/.mail_configs/.%s.psg" % server
  args = ["gpg", "--use-agent", "--quiet", "--batch", "-d", path]
  try:
    return subprocess.check_output(args).strip()
  except subprocess.CalledProcessError:
    return ""

