# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

# Prevent sending reboot to the wrong machine
alias reboot="echo Plz noooo, don\'t reboot me!"
alias myip="curl -s http://tnx.nl/ip\n"

#
# I want the number of current jobs in my prompt
#
function get_jobs {
  local res=`jobs |wc -l`
#  res=$((--res))
  if [ "$res" -eq 0 ];
  then
     echo "";
  else
     echo "$res ";
  fi
}


#
# I want to print the stack depth
#
function get_stack {
  local stack=`dirs -p| wc -l`
  if [ "$stack" -lt 2 ];
  then
     echo "";
  else
     echo "[$((stack-1))] "
  fi
}


#
# GIT specific (__git_ps1 is too slow)
#
export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true

function get_git_stack {
  local n=`git stash list 2> /dev/null |wc -l`
  if [[ $n -gt 0 ]];
  then
    echo -n "[$n]"
  else
    echo -n ""
  fi
}

function parse_git_branch {
    git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

function parse_git_status {
  noupdated=`git status --porcelain 2> /dev/null | grep -E "^ (M|D)" | wc -l`  nocommitted=`git status --porcelain 2> /dev/null | grep -E "^(M|A|D|R|C)" | wc -l`

   if [[ $noupdated -gt 0 ]];
   then
     echo -n "*";
   fi

   if [[ $nocommitted -gt 0 ]];
   then
      echo -n "+";
   fi
}


#
# setup right encoding for my yubikey. 
# ugly hack, should write an xorg conf file…
#
YUBI=`xinput --list |grep Yubikey |sed -n "s/.*id=\(.*\) .*/\1/p" |awk '{print $1}'`
if [ ! "@$YUBI" == "@" ]
then
  setxkbmap -device $YUBI us
fi



#
# Color
#
RED="\[\033[01;31m\]"
YELLOW="\[\033[01;33m\]"
B_GREEN="\[\033[01;32m\]"
GREEN="\[\033[00;32m\]"
BLUE="\[\033[01;34m\]"
NC="\[\033[0m\]"
BR="\[\033[00;33m\]"
GREY="\[\033[00;30m\]"

if [ ! "@$REMOTE" == "@" ]
then
   PS1="$GREEN\u@\h $BLUE\w$YELLOW\$(parse_git_branch) $BLUE\$ $NC"
else
    if [ ! "@$FULL_GIT" == "@" ]
    then
                # Depending of the repo, that could be really slow (more that 8 seconds!)
                PS1="${TITLEBAR}$B_GREEN\u $BR\$(get_stack)$BLUE\w\n$BLUE\$(get_jobs)$YELLOW\$(parse_git_branch)\$(parse_git_status)\$(get_git_stack)$BLUE\$ $NC"
        else
                PS1="${TITLEBAR}$B_GREEN\u $BR\$(get_stack)$BLUE\w\n$BLUE\$(get_jobs)$YELLOW\$(parse_git_branch)\$(get_git_stack)$BLUE\$ $NC"
    fi
fi

PS1="\[\033[00;30m\]\$(date +%T) $PS1"

# https://ask.fedoraproject.org/en/question/10685/bash-reverse-search-ctrlr-going-forward/
stty -ixon

