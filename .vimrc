set ts=4 sw=2
set tw=80
"set expandtab
set title
set splitbelow
set splitright
"set number
"set paste
"set autoindent
"set copyindent
"set formatoptions=croq
"set ignorecase

" display tabs, trailing space and non breaking spaces
set listchars=tab:→\ ,trail:·,extends:#,nbsp:_
set list

highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
" Show trailing whitespace and spaces before a tab
match ExtraWhitespace /\s\+$\| \+\ze\t/
" Show tabs that are not at the start of a line
match ExtraWhitespace /[^\t]\zs\t\+/

autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

map <F3> :set list
map <F5> :set nolist
"nnoremap <esc> :set nolist<return><esc>
map <F2> :md5sum m :%s/^\([;"#-/*]\+ Checksum:\).*/\1 m
map <F3> :%s/^\([;"#-/*]\+ Last change:\).*/\1 =strftime("%d %b %Y %X (%Z)")
map <F4> :%s/\s\+$//e

"autocmd BufWritePre * call feedkeys("\<F2>")
"autocmd BufWritePre * call feedkeys("\<F3>")

if !exists("format_tx_python")
  let format_tx_python = 1

  " Formatting Python code for Transifex.
  autocmd BufNewFile,BufRead *.py set autoindent showmatch
  autocmd BufNewFile,BufRead *.py set formatoptions=tcq2l
  autocmd BufNewFile,BufRead *.py set textwidth=78 shiftwidth=4
  autocmd BufNewFile,BufRead *.py set softtabstop=4 tabstop=8 expandtab
endif



au FileType c,h setlocal cindent
au FileType java,js setlocal smartindent
au FileType txt setlocal fo+=tn

" enable sh syntax highlighting from shebang!
if ! did_filetype()
  if getline(1) =~# '^#!.*/bin/sh\>'
    setfiletype sh
  endif
endif

if has("autocmd")
  au BufReadPost *.bb set syntax=sh
  au BufReadPost *.bbclass,*.inc,*.bbappend set syntax=python
endif

" more at http://apaulodesign.com/vimrc.html
"

highlight Search ctermbg=black ctermfg=yellow

